﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;

namespace DungeonMaster
{
    /// <summary>
    /// Handles dependency injection
    /// </summary>
    public class Initialize
    {
        private readonly CommandService Commands;
        private readonly DiscordSocketClient Client;

        public Initialize(
            CommandService commands = null,
            DiscordSocketClient client = null)
        {
            Client = client ?? new DiscordSocketClient();
            Commands = commands ?? new CommandService();
        }

        public IServiceProvider BuildServiceProvider()
        {
            return new ServiceCollection()
                .AddSingleton(Client)
                .AddSingleton(Commands)
                .BuildServiceProvider();
        }  
    }

    /// <summary>
    /// Handles command installation and running
    /// </summary>
    public class CommandHandler
    {
        private readonly DiscordSocketClient Client;
        private readonly CommandService Commands;
        private readonly IServiceProvider Services;

        public CommandHandler(
            DiscordSocketClient client,
            CommandService commands,
            IServiceProvider services)
        {
            Client = client;
            Commands = commands;
            Services = services;
        }

        public async Task InstallCommands()
        {
            // Init client message received
            Client.MessageReceived += HandleCommand;

            await Commands.AddModulesAsync(
                assembly: Assembly.GetEntryAssembly(),
                services: Services);
        }

        private async Task HandleCommand(SocketMessage msg)
        {
            SocketUserMessage message = msg as SocketUserMessage;
            if (message == null) return;

            int prefixPos = 0;

            // Validate message
            if (!(message.HasCharPrefix('!', ref prefixPos) ||
                message.HasMentionPrefix(Client.CurrentUser, ref prefixPos)) ||
                message.Author.IsBot)
            {
                return;
            }

            // Attempt to execute command
            SocketCommandContext context = new SocketCommandContext(Client, message);
            var result = await Commands.ExecuteAsync(
                context: context,
                argPos: prefixPos,
                services: Services
            );

            // Handle command success/failure
            if (!result.IsSuccess)
            {
                Console.WriteLine($"{DateTime.Now.ToString("HH:mm:ss")} {message.Author} tried to run '{message.Content}'" +
                    $"\n\t{result.ErrorReason}");
            }
            else
            {
                Console.WriteLine($"{DateTime.Now.ToString("HH:mm:ss")} {message.Author} ran '{message.Content}'");
            }
        }
    }
}
