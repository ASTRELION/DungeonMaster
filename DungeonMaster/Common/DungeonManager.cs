﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonMaster
{
    /// <summary>
    /// Handles creation, management, and deletion of dungeons
    /// </summary>
    public class DungeonManager
    {
        /// <summary>
        /// Valid monster names
        /// </summary>
        public static readonly string[] MONSTER_NAMES =
        {
            "Cultist",
            "Fire Mage",
            "Water Mage",
            "Air Mage",
            "Earth Mage",
            "Ghoul",
            "Giant Spider",
            "Goblin",
            "Orc",
            "Orge",
            "Giant Bear",
            "Giant Snake",
            "Hydra",
            "Skeleton",
            "Wolf",
            "Zombie",
            "Giant Bat",
            "Fire Dragon",
            "Reaper",
            "Troll",
            "Assassin",
            "Bandit",
            "Corgi",
            "Wumpus",
            "Giant Crap",
            "Ebony Knight",
            "Dovahkiin",
            "Vampire"
        };


        private readonly string img =
            "  ,/         \\. \n" +
            " ((           )) \n" +
            "  \\`.       ,'/ \n" +
            "   )')     (`( \n" +
            " ,'`/       \\,`. \n" +
            "(`-(         )-') \n" +
            " \\-'\\,-'\"`-./`-/ \n" +
            "  \\-')     (`-/ \n" +
            "  /`'       `'\\ \n" +
            " (  _       _  ) \n" +
            " | ( \\     / ) | \n" +
            " |  `.\\   /,'  | \n" +
            " |    `\\ /'    | \n" +
            " (             ) \n" +
            "  \\           / \n" +
            "   \\         / \n" +
            "    `.     ,' \n" +
            "      `-.-' ";

        /// <summary>
        /// Emotes
        /// </summary>
        public static readonly Emoji[] EMOTES =
        {
            new Emoji("\U0001F5E1"), // Dagger
            new Emoji("\U0001F3F9"), // Bow
        };

        /// <summary>
        /// Generates the embed message representing the monster
        /// </summary>
        /// <returns>Embed message</returns>
        public Embed GenerateMonsterCard(SocketCommandContext Context)
        {
            GuildConfig config = new GuildConfig(Context.Guild.Id.ToString());
            config.CheckForFile();
            config = config.Load() as GuildConfig;

            Random rand = new Random();
            long monsterHealth = (long)(Math.Pow(config.DungeonLevel, 1.2) + 5 + rand.Next(10));
            string monsterName = MONSTER_NAMES[rand.Next(MONSTER_NAMES.Length)];

            EmbedBuilder monsterCard = new EmbedBuilder()
            {
                Title = monsterName + " lvl " + config.DungeonLevel,
                Description = $"{monsterHealth} / {monsterHealth}\n```{img}```",
                Color = new Color(0, 255, 0),
            };

            config.Save();
            return monsterCard.Build();
        }

        public Embed RefreshMonsterCard(IEmbed mc, SocketGuild guild, SocketGuildUser user)
        {
            Random rand = new Random();

            // Configuration
            GuildConfig config = new GuildConfig(guild.Id.ToString());
            config.CheckForFile();
            config = config.Load() as GuildConfig;

            // Embed info
            EmbedBuilder monsterCard = mc.ToEmbedBuilder();
            string line = monsterCard.Description.Split('\n')[0];
            long currentHealth = Convert.ToInt64(line.Split('/')[0]);
            long maxHealth = Convert.ToInt64(line.Split('/')[1]);

            // Channels
            SocketTextChannel dungeonChannel = guild.GetTextChannel(config.DungeonChannelID);
            IUserMessage log = dungeonChannel.GetMessageAsync(config.DungeonCombatID).Result as IUserMessage;

            // Document action
            // Calculate damage
            int damage = rand.Next(6) + 1;

            bool crit = false;
            int critRoll = rand.Next(6) + 1;

            if (critRoll == 6)
            {
                crit = true;
                damage *= 2;
            }

            currentHealth -= damage;
            config.DungeonCombatLog += $"\n{DateTime.Now.ToString("HH:mm:ss")}|{user} {(crit ? "crit for" : "dealt")} {damage} damage!";

            // Slain monster
            if (currentHealth <= 0)
            {
                // Document action
                config.DungeonLevel++;
                config.DungeonCombatLog += $"\n{DateTime.Now.ToString("HH:mm:ss")}|{user} slew the {monsterCard.Title}!";

                // Generate new monster
                long monsterHealth = (long)(Math.Pow(config.DungeonLevel, 1.2) + 5 + rand.Next(10));
                string monsterName = MONSTER_NAMES[rand.Next(MONSTER_NAMES.Length)];

                EmbedBuilder newMonsterCard = new EmbedBuilder()
                {
                    Title = monsterName + " lvl " + config.DungeonLevel,
                    Description = $"{monsterHealth} / {monsterHealth}\n```{img}```",
                    Color = new Color(0, 255, 0),
                };

                log.ModifyAsync(x => x.Content = $"```{config.FormatedCombatLog()}```");

                config.Save();
                return newMonsterCard.Build();
            }

            log.ModifyAsync(x => x.Content = $"```{config.FormatedCombatLog()}```");

            monsterCard.Description = $"{currentHealth} / {maxHealth}\n```{img}```";

            // Gradient the color from green to red based on remaining health
            Color healthColor = new Color(0, 255, 0);
            float healthRatio = (float)currentHealth / (float)maxHealth;

            if (healthRatio >= 0.5)
            {
                healthColor = new Color((int)(255 * (2 - (2 * healthRatio))), 255, 0);
            }
            else
            {
                healthColor = new Color(255, (int)(255 * (2 * healthRatio)), 0);
            }
            
            monsterCard.Color = healthColor;

            // Save and return
            config.Save();
            return monsterCard.Build();
        }
    }
}
