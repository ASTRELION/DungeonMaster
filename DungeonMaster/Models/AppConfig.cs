﻿using System;
using System.IO;

namespace DungeonMaster
{
    /// <summary>
    /// Configuration model for the Bot Application
    /// </summary>
    public class AppConfig : Config
    {
        public AppConfig(string fileName) : base(fileName)
        {
            
        }

        public AppConfig()
        {

        }

        /// <summary>
        /// Discord Bot token
        /// </summary>
        public string Token { get; set; }
        public string AppName { get; set; }
        public string Version { get; set; }

        public override void CheckForFile()
        {
            if (!File.Exists(BasePath + FileName))
            {
                AppConfig config = new AppConfig("_Config.xml");

                Console.Write("Token: ");
                string token = Console.ReadLine();

                config.Token = token;
                config.AppName = "Dungeon Master";
                config.Version = "0.0.1";
                config.Save();
            }
            else
            {
                AppConfig config = new AppConfig("_Config.xml");
                config = config.Load() as AppConfig;

                config.Token = config.Token;
                config.AppName = "Dungeon Master";
                config.Version = "0.0.1";
                config.Save();
            }
        }
    }
}
