﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace DungeonMaster
{
    [XmlInclude(typeof(AppConfig))]
    [XmlInclude(typeof(GuildConfig))]
    public abstract class Config
    {
        protected readonly string BasePath = AppContext.BaseDirectory + "/Data/";

        /// <summary>
        /// File path to save configuration file
        /// </summary>
        public string FileName { get; set; }

        public Config()
        {

        }

        public Config(string fileName)
        {
            FileName = fileName + ".xml";
        }

        /// <summary>
        /// Create file if it does not exist yet
        /// </summary>
        public abstract void CheckForFile();

        /// <summary>
        /// Save config to file
        /// </summary>
        public void Save()
        {
            XmlSerializer serial = new XmlSerializer(typeof(Config));
            TextWriter writer = new StreamWriter(BasePath + FileName);
            serial.Serialize(writer, this);
            writer.Close();
        }

        /// <summary>
        /// Delete entire configuration file
        /// This requires CheckForFile() to be called again for file to be accessed
        /// </summary>
        public void Delete()
        {
            File.Delete(BasePath + FileName);
        }

        /// <summary>
        /// Load configuration file into Config object
        /// </summary>
        /// <returns>Config object</returns>
        public Config Load()
        {
            XmlSerializer serial = new XmlSerializer(typeof(Config), new Type[] {typeof(AppConfig)});
            TextReader reader = new StreamReader(BasePath + FileName);
            object obj = serial.Deserialize(reader);
            Config config = obj as Config;
            reader.Close();

            return config;
        }
    }
}
