﻿using System;
using System.IO;

namespace DungeonMaster
{
    /// <summary>
    /// Configuration model for each Guilds' Information and Settings
    /// </summary>
    public class GuildConfig : Config
    {
        /// <summary>
        /// ID representing the server
        /// </summary>
        public ulong GuildID { get; set; }
        /// <summary>
        /// ID of the channel that the dungeon exists in
        /// </summary>
        public ulong DungeonChannelID { get; set; }
        /// <summary>
        /// ID of the message that the dungeon monsters are displayed on
        /// </summary>
        public ulong DungeonMessageID { get; set; }
        /// <summary>
        /// ID of the message that the combat log should be displayed on
        /// </summary>
        public ulong DungeonCombatID { get; set; }
        /// <summary>
        /// Determines difficulty of monsters, starts at 0
        /// </summary>
        public long DungeonLevel { get; set; }
        /// <summary>
        /// Combat log against the current level monster
        /// </summary>
        public string DungeonCombatLog { get; set; }

        public GuildConfig(string fileName) : base(fileName)
        {
            FileName = "g_" + fileName + ".xml";
            GuildID = Convert.ToUInt64(fileName);
        }

        public GuildConfig()
        {

        }

        public override void CheckForFile()
        {
            // eg. "g_262726242036350976.xml"
            if (!File.Exists(BasePath + FileName))
            {
                GuildConfig config = new GuildConfig(GuildID.ToString());

                config.GuildID = config.GuildID;
                config.DungeonCombatLog = "\u200B\n\u200B\n\u200B\n\u200B\n\u200B";

                config.Save();
            }
            else
            {
                GuildConfig config = Load() as GuildConfig;
                config.Save();
            }
        }

        /// <summary>
        /// Returns 5 line version of the combat log
        /// </summary>
        public string FormatedCombatLog()
        {
            string[] logArray = DungeonCombatLog.Split('\n');
            string newLog = "";

            for (int i = logArray.Length - 5; i < logArray.Length; i++)
            {
                newLog += "\n" + logArray[i];
            }

            return newLog;
        }
    }
}
