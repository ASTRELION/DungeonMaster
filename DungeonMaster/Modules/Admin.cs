﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace DungeonMaster
{
    public class Admin : ModuleBase<SocketCommandContext>
    {
        

        [Command("createdungeon")]
        [Summary("Creates a new dungeon")]
        public async Task CreateDungeon()
        {
            DungeonManager manager = new DungeonManager();
            GuildConfig config = new GuildConfig(Context.Guild.Id.ToString());
            config.CheckForFile();
            config = config.Load() as GuildConfig;

            if (config.DungeonChannelID != Context.Channel.Id)
            {
                await ReplyAsync("*Command not ran in dungeon, creating dungeon in this channel...*");
                config.DungeonChannelID = Context.Channel.Id;
            }

            var monsterMessage = await ReplyAsync(embed: manager.GenerateMonsterCard(Context));
            config.DungeonMessageID = monsterMessage.Id;

            await monsterMessage.AddReactionsAsync(DungeonManager.EMOTES);

            var combatMessage = await ReplyAsync($"```{config.DungeonCombatLog}```");
            config.DungeonCombatID = combatMessage.Id;

            config.Save();
        }
    }
}
