﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace DungeonMaster
{
    public class General : ModuleBase<SocketCommandContext>
    {
        public DiscordSocketClient Client { get; set; }
        public CommandService Commands { get; set; }

        [Command("ping")]
        [Summary("Pings the bot")]
        public async Task PingAsync()
        {
            await ReplyAsync($"Pong! *{Client.Latency}ms*");
        }

        [Command("info")]
        [Summary("Get information about the Dungeon Master")]
        public async Task Info()
        {
            IApplication app = await Client.GetApplicationInfoAsync();

            // Build info page
            EmbedBuilder info = new EmbedBuilder()
            {
                Title = $"{app.Name} Information",
                Description = $"**Name:** {app.Name}" +
                              $"\n**Author:** {app.Owner.Username}" +
                              $"\n**Description:** {app.Description}" +
                              $"\n**Date Created:** {app.CreatedAt}" +
                              $"\n**Guilds Joined:** {Client.Guilds.Count}",
                Color = new Color(255, 255, 255)
            };
            
            await ReplyAsync("", embed: info.Build());
        }

        [Command("help")]
        [Summary("Get help on a specific command or list all commands")]
        [Alias("?")]
        public async Task Help(
            [Summary("Command to get more information about"), Remainder] string command = null)
        {
            // Weed out irrelevant commands
            List<CommandInfo> cmds = Commands.Commands.Where(x => x.CheckPreconditionsAsync(Context).Result.IsSuccess).ToList();

            if (command != null)
            {
                cmds = Commands.Commands.Where(x => x.Name.ToLower().Equals(command.ToLower())).ToList();
            }

            // Build help page
            EmbedBuilder helpPage = new EmbedBuilder()
            {
                Title = $"Help",
                Description = "\u200B",
                Color = new Color(255, 255, 255)
            };

            // Add commands to page
            foreach (CommandInfo cInfo in cmds)
            {
                helpPage.Description += GetCommandSyntax(cInfo);

                if (cInfo.Summary != null)
                    helpPage.Description += $" : {cInfo.Summary}\n";
            }

            helpPage.Description += "\n*You can message me directly to run most player commands*";

            await ReplyAsync("", embed: helpPage.Build());
        }

        private string GetCommandSyntax(CommandInfo command)
        {
            string syntax = $"!{command.Aliases[0]}";

            // Format parameters with [] and <>
            foreach (ParameterInfo param in command.Parameters)
            {
                if (param.IsOptional)
                {
                    syntax += $"[{param.Name}] ";
                }
                else if (!param.IsOptional)
                {
                    syntax += $"<{param.Name}> ";
                }
            }

            return $"**{syntax}**";
        }
    }
}
