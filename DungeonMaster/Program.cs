﻿using System;
using System.Threading.Tasks;
using System.Linq;
using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace DungeonMaster
{
    class Program
    {
        public static void Main(string[] args)
            => new Program().MainAsync().GetAwaiter().GetResult();

        private DiscordSocketClient Client;

        public async Task MainAsync()
        {
            // Load configuration
            AppConfig config = new AppConfig("_Config.xml");
            config.CheckForFile();
            config = config.Load() as AppConfig;
            
            // Init client
            Client = new DiscordSocketClient();
            Client.Log += LogEvent;
            Client.Ready += ReadyEvent;
            Client.ReactionAdded += ReactionAddedEvent;

            // Init commands
            CommandService commands = new CommandService();

            Initialize injection = new Initialize(commands, Client);
            IServiceProvider provider = injection.BuildServiceProvider();
            CommandHandler handler = new CommandHandler(Client, commands, provider);

            await handler.InstallCommands();

            // Start client
            await Client.LoginAsync(TokenType.Bot, config.Token);
            await Client.StartAsync();

            await Task.Delay(-1);
        }

        /// <summary>
        /// Simple log event
        /// </summary>
        /// <param name="msg">Log message</param>
        /// <returns></returns>
        private Task LogEvent(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());

            return Task.CompletedTask;
        }

        /// <summary>
        /// Housekeeping functions to run just as bot comes online
        /// </summary>
        /// <returns></returns>
        private async Task ReadyEvent()
        {
            // Update bot name if app name is different
            IApplication app = await Client.GetApplicationInfoAsync();
            await Client.CurrentUser.ModifyAsync(x =>
            {
                x.Username = app.Name;
            });

            // Set bot status message
            await Client.SetGameAsync($"!help");
        }

        private async Task ReactionAddedEvent(Cacheable<IUserMessage, ulong> message, ISocketMessageChannel channel, SocketReaction reaction)
        {
            IUserMessage msg = message.DownloadAsync().Result;
            SocketGuildUser user = await channel.GetUserAsync(reaction.UserId) as SocketGuildUser;
            SocketGuild guild = user.Guild;
            DungeonManager manager = new DungeonManager();

            GuildConfig config = new GuildConfig(guild.Id.ToString());
            config.CheckForFile();
            config = config.Load() as GuildConfig;
            
            if (config.DungeonChannelID != channel.Id || config.DungeonMessageID != message.Id ||
                user.IsBot)
            {
                return;
            }
            
            // Check if valid emote
            for (int i = 0; i < DungeonManager.EMOTES.Length; i++)
            {
                if (DungeonManager.EMOTES[i].Equals(reaction.Emote))
                {
                    break;
                }
                else if (i == DungeonManager.EMOTES.Length - 1)
                {
                    return;
                }
            }

            Embed newEmbed = manager.RefreshMonsterCard(msg.Embeds.First(), guild, user);
            await msg.ModifyAsync(x => x.Embed = newEmbed);
            await msg.RemoveReactionAsync(reaction.Emote, reaction.User.Value);
        }
    }
}
