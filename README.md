# Dungeon Master
Dungeon Master is a Discord server bot created during the *1st [Discord](https://discordapp.com/) Community Hack Week*. Dungeon Master aims at creating a dungeon-like experience for the entire server to participate in with goals of achieving (but not limited to):
- Server-wide combat on respawning dungeon monsters
- Random loot
- Player inventory system
- Player health, combat, and damage
- Simple class system

## Installation & Getting Started

1. Clone or download the project
2. Open project solution
3. **Ensure DungeonMaster/bin/Debug/Data exists before running**
4. Run console app via solution
5. Enter your bot key & add the bot to your server
6. Enjoy!

### Getting Started

1. Run `!ping` to make sure the bot is running
2. Run `!createdungeon` in an empty text channel
3. A dungeon will be created
4. React to the monster with the sword or bow emoji to deal damage
5. Slay monsters to advance in level!

## Current Features

- Creation of randomly named monsters of exponentially growing health
- Player reactions will deal a random amount of damage (1-6) with a 1/6 chance of a crit
- Monster is displayed via an embed that will chance color depending on monster health
- Combat log keeps track of all damage dealt and monsters slain
- Monster level indicates difficulty
- Entire combat log can be found in configuration file for your server

#

**Created by ASTRELION**

## Commands
*:warning:  indicates a command that hasn't been implemented yet*
*This section is intended to be used as a framework for where the bot* will *be, not its current state*

### Administrative
- `!createdungeon` creates a dungeon in the current chat channel. *The Dungeon Master will be the only user allowed to write in the channel; only 1 dungeon may be active in a server at once*
- :warning:`!pausedungeon` pauses the dungeon, not allowing any further action against monsters or players to persist
- :warning:`!playdungeon` unpauses the dungeon if paused
- :warning:`!resetdungeon` resets the dungeon of the server if it exists, sets the dungeon to level 0
- :warning:`!destroydungeon` resets and removes the dungeon from the server
- :warning:`!createspells` create the Spells channel, allowing players to run commands in *this channel* and direct messages. *Unlimited spell channels may be specified*
- :warning:`!destroyspells` destroys the current Spells channel if it exists, not allowing further spells to be cast

### Player
*All player commands **must** be used in a direct message channel with The Dungeon Master or in the Spells chat channel if specified. If a command is used in an invalid server, it will be deleted and the commands result will be DM'd to the player. All player stats and inventory are shared throughout every server the bot instance is running on*
- :warning:`!stats` display all stats including inventory, health, level, abilities, and modifiers
- :warning:`!inventory` display your inventory
- :warning:`!drop <int>` drop inventory item in slot indicated, effectively destroying the item
- :warning:`!give <int> <string/@>` give inventory item in slot indicated to indiciated player
- :warning:`!level` display current level and xp remaining to next level
- :warning:`!levelup` advance to the next level. *Requires xp threshold to be met; This command will level you up multiple times in a single command if xp is high enough to advance multiple levels*
- :warning:`!health` display current health
- :warning:`!reset` sets the player back to level 0, destroys all inventory, and resets all stats and modifiers
- :warning:`!leaderboard [int]` display the player leaderboard of the server, sorted by highest to lowest xp

### General Use
- `!ping!` ping the Dungeon Master; displays latency
- `!info` get info about the Dungeon Master and author
- `!help [string]` get a list all commands you have access to or a specific command
